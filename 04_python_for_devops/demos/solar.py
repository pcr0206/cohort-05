#!/usr/bin/env python3

import requests
import datetime

ASTRONOMYAPI_ID="39011599-7c68-42a1-954e-8ac5e6bc91cd"
ASTRONOMYAPI_SECRET="e951e0b6337bc1d6e60f371cde3f5e61ca46c621f949ab161122f9a33bf737e9de66d00e5c7715df3540f36e0fd0e6854761c9faa8e158269acd6e500726d9b10f8d55da7f5a729e5e4a12f7c7c848a20f3583433802a68c462023fbf90be473d4c83ac9958530e1b5ee30d471372751"

def get_observer_location():
    """Returns the longitude and latitude for the location of this machine.
    Returns:
        str: latitude
        str: longitude"""

    #NOTE: Replace with your real return values!
    return None, None

def get_sun_position(latitude, longitude):
    """Returns the current position of the sun in the sky at the specified location

    Parameters:
    latitude (str)
    longitude (str)

    Returns:
        float: azimuth
        float: altitude
        """

    #NOTE: Replace with your real return values!
    return 0, 0


def print_position(azimuth, altitude):
    """Prints the position of the sun in the sky using the supplied coordinates

    Parameters:
        azimuth (float)
        altitude (float)
        """

    print("The Sun is currently at:")

if __name__=="__main__":
    latitude, longitude = get_observer_location()
    azimuth, altitude = get_sun_position(latitude, longitude)
    print_position(azimuth, altitude)




