def calculate(no1, no2, operator) -> float:
    """
    This is the calculation for OS level
    provided digits and operator.
    """
    if operator == '+':
        return(no1+no2)
    elif operator == '-':
        return(no1-no2)
    elif operator == '*':
        return(no1*no2)
    elif operator == '/':
        try:
            return(no1/no2)
        except ZeroDivisionError:
            print('Number cannot divide by :', no2)
    else:
        return "Error: Not a valid operator!"

