#!/usr/bin/env python3

import sys

def calculate(no1, no2, operator) -> float: 
    if operator == '+':
        return(no1+no2)
    elif operator == '-':
        return(no1-no2)
    elif operator == '*':
        return(no1*no2)
    elif operator == '/':
        try:
            return(no1/no2)
        except ZeroDivisionError:
            print('Number cannot divide by :', no2)
    else:
        return "Error: Not a valid operator!"


args = sys.argv

print('Arguments are: ', args)

result = calculate(int(args[1]), int(args[2]), args[3])
#OR
# result = calculate(int(sys.argv[1]), int(sys.argv[2]), sys.argv[3])
print('Two number calculation', result)
