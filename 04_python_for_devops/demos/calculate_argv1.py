#!/usr/bin/env python3

#Using the slicing

import sys

def calculate(no1, no2, operator) -> float: 
    if operator == '+':
        return(no1+no2)
    elif operator == '-':
        return(no1-no2)
    elif operator == '*':
        return(no1*no2)
    elif operator == '/':
        try:
            return(no1/no2)
        except ZeroDivisionError:
            print('Number cannot divide by :', no2)
    else:
        return "Error: Not a valid operator!"


x, y, op = sys.argv[1:]

print('Arguments are: ', sys.argv)

result = calculate(int(x), int(y), op)
#OR
# result = calculate(int(sys.argv[1]), int(sys.argv[2]), sys.argv[3])
print('Two number calculation', result)
