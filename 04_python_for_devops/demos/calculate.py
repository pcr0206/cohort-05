#!/usr/bin/env python3

import sys
import os
import def_cal

args = sys.argv

#print(os.def_cal.py.__doc__)

print('Arguments are: ', args)

result = def_cal.calculate(int(args[1]), int(args[2]), args[3])
#OR
# result = calculate(int(sys.argv[1]), int(sys.argv[2]), sys.argv[3])
print('Two number calculation', result)
